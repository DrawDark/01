import * as React from 'react';
import { Slider, View,Text, TextInput,ScrollView ,Switch } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { HomeStack } from "./src/HomeStack";
import { SearchStack } from "./src/SearchStack";
import { PerfilStack } from "./src/PerfilStack";
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import MultiSlider from '@ptomasroos/react-native-multi-slider';






const Tab = createMaterialTopTabNavigator();

function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, 
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:'#FD5068' }}>
          <View style={{flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'center'}}>
          <MaterialCommunityIcons name="fire" size={80} color='#fff'/> 
          <Text style={{  fontSize: 50,    color:'#ffff' }}>tinder</Text>
          </View>
          <View style={{
            height:50
          }}></View>
          <Text style={{  fontSize: 15,    color:'#ffff' }}>By tapping Log In, you agree with</Text>
          <Text style={{  fontSize: 15,    color:'#ffff' }}>our Terms of Service and Privacy</Text>
          <Text style={{  fontSize: 15,    color:'#ffff' }}>Policy</Text>
          <View style={{
            height:30
          }}></View>
  
          <View style={{
            alignItems: 'center',
            justifyContent: 'center',
            width:350,
            height:65,
            backgroundColor:'#3b5998',
            borderRadius:30
          }}
          >
  <Text style={{  fontSize: 20,    color:'#ffff' }}    onPress={() => {    navigation.navigate("Profile")     }}  >LOG IN WITH FACEBOOK</Text>
  
          </View>
          <View style={{
            height:10
          }}></View>
  
          <View style={{
            alignItems: 'center',
            justifyContent: 'center',
            width:350,
            height:65,
            borderWidth: 3,
            borderColor:'#FFFF',
            borderRadius:30
          }}
          >
  <Text style={{  fontSize: 20,    color:'#ffff' }}    onPress={() => {    navigation.navigate("Forget")    }}  >LOG IN WITH PHONE NUMBER</Text>
  
          </View>
          <View style={{
            height:50
          }}></View>
          <Text style={{  fontSize: 15,    color:'#ffff' }}>Trouble Logging in?</Text>
          <View style={{
            height:30
          }}></View>
  
          <Text style={{  fontSize: 15,    color:'#ffff' }}>We don't post anything to</Text>
          <Text style={{  fontSize: 15,    color:'#ffff' }}>Facebook</Text>
  
          
        
       
    </View>

     //<Button      title="Go to Profile"       onPress={() => navigation.navigate('Profile')}      />
     //<Button  title="Go to Notifications" onPress={() => navigation.navigate('Notifications')} />
  );
}

function ProfileScreen({ navigation }) {
    
  return (
    
       <Tab.Navigator 
      
        tabBarOptions={{
          indicatorStyle:{backgroundColor:'#fff'},
          style:{elevation: 0,borderBottomWidth:1,borderBottomColor:'#ededed' },
        showIcon: true, 
        showLabel:false,
        activeTintColor: "tomato",
        inactiveTintColor: "gray",
        
        
        
      }}>

        <Tab.Screen name="Perfil" component={PerfilStack} 
        options={{
          
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="account" color={color} size={30} />
          ),
        }}/>
        <Tab.Screen name="Main" component={HomeStack}
         options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="heart" color={color} size={26} />
          ),
        }}/>
        <Tab.Screen name="Contatos" component={SearchStack}  options={{
          tabBarLabel: 'Contatos',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="chat" color={color} size={26} />
          ),
        }}/>
      </Tab.Navigator>
     
    
  );
}

function Forget({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', backgroundColor:'#ffff' }}>
      <View style={{
        height:50
      }}>

      </View>
      <View style={{
        width:350,
        height:100
        
      }}>
<Text style={{ fontSize: 20, color:'#000' }}>Meu número é</Text>
      </View>

      
        
       <View style={{flexDirection:'row',
          alignItems: 'center',
          justifyContent: 'center',}}>
      <Text title="EmailRecuperar" 
                   style={{
                   width: 60,
                   height: 40,                        
                   backgroundColor:'#ffff',
                   fontSize:20
                  }}>
  
                  +55 <MaterialCommunityIcons name="chevron-down" size={20} color='#000'/>
                  </Text>
                  <View style={{
                    width:30,
                    height:3,
                    
                  }}></View>

        <TextInput title="EmailRecuperar" 
                   style={{ width: 250,
                   height: 40,                        
                   backgroundColor:'#ffff',
                   fontSize:20 
                  }}>
  
                  Número de telefone
                  </TextInput></View>
                  <View style={{flexDirection:'row',
          alignItems: 'center',
          justifyContent: 'center',}}>
            
            <View style={{
                    width:60,
                    height:3,
                    backgroundColor:'#000'
                  }}></View>
                  <View style={{
                    width:30,
                    height:3,
                    
                  }}></View>
                  <View style={{
                    width:250,
                    height:3,
                    backgroundColor:'#000'
                  }}></View>
                  </View>
                  <View style={{
                    width:380,
                    height:30}}></View>
                  <View style={{
                    width:350,
                    height:150,
                    
                  }}><Text>Quando você tocar em Continuar, o Tinder lhe enviará </Text>
                  <Text>uma mensagem de texto com o código de verificação. </Text>
                  <Text>Tarifas de mensagens e dados podem ser aplicáveis. O </Text>
                  <Text>número de telefone confirmado pode ser utilizado para </Text>
                  <Text>entrar no Tinder. Saiba o que acontece se seu número mudar.</Text>
                  </View>
  
                  <View style={{
          alignItems: 'center',
          justifyContent: 'center',
          width:350,
          height:65,
          borderWidth: 3,
          borderColor:'#000',
          borderRadius:30
        }}
        >
<Text style={{  fontSize: 20,    color:'#000' }}    onPress={() => {    navigation.navigate("HomeScreen")    }}  >CONTINUAR</Text>

        </View>
        </View>
    
  );
}

function SettingsScreen({navigation }) {
  return (
    <View><View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor:'#fff',borderBottomWidth:1,borderColor:'#F6F7FB',
        width:450,
        height:49}}> 
        <View style={{justifyContent: 'center', alignItems: 'center',  width:60,   height:100}}>
        <MaterialIcons name="arrow-back" size={25} color='tomato' onPress={() => navigation.goBack()}/>
        </View>
        
        <Text style={{fontSize:20,marginLeft:15,}}>Configurações</Text>
                
            </View>
        
        <ScrollView style={{backgroundColor:'#f5f5f5'}}>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
                
                    
                     
                    <View>
                    <View><Text style={{fontSize:17,fontWeight: "bold",margin:5, }}> Configurações da conta</Text>
                   <View style={{justifyContent: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:60,
                    marginLeft: 5,
                    borderRadius:5
                   }}>
                    
                   <Text style={{fontSize:17,margin:10 }}>Número de telefone</Text>
                   <Text style={{color:'grey',fontSize:20,margin:10,marginLeft:1}}>55 00 00000-0000</Text></View>
                   <View >
                   <Text style={{marginLeft: 10, color:'grey'}}>Verifique seu número de telefone para ajudar a proteger</Text>
                   <Text style={{marginLeft: 10, color:'grey'}}>a sua conta.</Text></View>
                   </View>

                   <View><Text style={{fontSize:17,fontWeight: "bold",margin:5 }}> Ajustes de descoberta</Text>
                   <View style={{justifyContent: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:60,
                    marginLeft: 5,
                    borderRadius:5
                   }}>
                    
                   <Text style={{fontSize:17,margin:15 }}> Localização</Text>
                   <Text style={{color:'blue',fontSize:17,margin:15,marginLeft:90}}>Localização atual</Text></View>
                   <Text style={{marginLeft: 10, color:'grey'}}>Altere a sua localização e veja membros do Tinder de</Text>
                   <Text style={{marginLeft: 10, color:'grey'}}>outras cidades.</Text>
                   </View>

                   <View><View>
                   <View style={{alignItems: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    marginTop:10,
                    borderTopLeftRadius:5,
                    borderTopLeftRadius:5
                    
                   }}>
                    
                    <Text style={{fontSize:17,fontWeight: "bold",margin:10,marginLeft:10,color:'#ff5864' }}> Mostre-me</Text>
                   </View>
                   <View style={{justifyContent: 'center', flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    
                    borderBottomLeftRadius:5,
                    borderBottomRightRadius:5
                   }}>
                    
                   <Text style={{fontSize:17,margin:5 }}>Mulheres</Text>
                   <Text style={{color:'grey',fontSize:20,margin:5,marginLeft:240}}>></Text></View></View>
                   </View>
                   

                   <View><View>
                   <View style={{alignItems: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    borderTopLeftRadius:5,
                    borderTopLeftRadius:5,
                    marginTop:20
                   }}>
                    
                    <Text style={{fontSize:17,fontWeight: "bold",margin:10,
                    marginLeft:15,color:'#ff5864' }}>Distância máxima</Text>
                    <Text style={{color:'black',fontSize:20,margin:5,marginLeft:135,fontWeight: "bold"}}>26km</Text>
                   </View>
                   <View style={{justifyContent: 'center', flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    
                    borderBottomLeftRadius:5,
                    borderBottomRightRadius:5
                   }}>
                    
                    <Slider            thumbTintColor={'#ff5864'}
                    maximumTrackTintColor={'#ff5864'}
                    minimumTrackTintColor={'#ff5864'}
                    style={{width:300,}}            
                    minimumValue={0}            
                    maximumValue={100}    />
                   </View></View>
                   </View>


                   <View><View>
                   <View style={{alignItems: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    borderTopLeftRadius:5,
                    borderTopLeftRadius:5,
                    marginTop:20
                   }}>
                    
                    <Text style={{fontSize:17,fontWeight: "bold",margin:10,
                    marginLeft:15,color:'#ff5864' }}>Faixa etária</Text>
                    <Text style={{color:'black',fontSize:20,margin:5,marginLeft:165,fontWeight: "bold"}}>18 - 24</Text>
                   </View>
                   <View style={{justifyContent: 'center', flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:350,
                    height:45,
                    marginLeft: 5,
                    
                    borderBottomLeftRadius:5,
                    borderBottomRightRadius:5
                   }}>
                    
                    <Slider            thumbTintColor={'#ff5864'}
                    maximumTrackTintColor={'#ff5864'}
                    minimumTrackTintColor={'#ff5864'}
                    style={{width:300,}}            
                    minimumValue={0}            
                    maximumValue={100}    />
    


                   </View></View>
                   <Text style={{marginLeft: 10, color:'grey'}}>O Tinder usa essas preferências para sugerir Matches.</Text>
                   <Text style={{marginLeft: 10, color:'grey'}}>Algumas sugestões podem não estar dentro dos seus</Text>
                   <Text style={{marginLeft: 10, color:'grey'}}>parâmetros ideais.</Text>
                   </View>


                   <View>
                   <View style={{justifyContent: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:60,
                    marginLeft: 5,
                    borderRadius:5,
                    marginTop:20
                   }}>
                    
                   <Text style={{fontSize:17,margin:15,marginRight:140 }}> Mostre-me no Tinder</Text>
                   <Switch/></View>
                   <Text style={{marginLeft: 10, color:'grey'}}>Você não aparecerá na coleção de cartões, mas ainda</Text>
                   <Text style={{marginLeft: 10, color:'grey'}}>poderá enviar mensagens para os Maches já existentes.</Text>
                   </View>


                   <View>
                   <View style={{alignItems: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:60,
                    marginLeft: 5,
                    borderRadius:5,
                    marginTop:20
                   }}>
                    
                   <Text style={{fontSize:17,margin:15 }}>Compartilhar meu Feed</Text>
                   </View>
                   <Text style={{marginLeft: 10, color:'grey'}}>Ao compartilhar se conteúdo das redes sociais, suas</Text>
                   <Text style={{marginLeft: 10, color:'grey'}}>chances de receber mensagens aumentam</Text>
                   <Text style={{marginLeft: 10, color:'grey'}}>concideravelmente!</Text>
                   </View>


                   <View>
                   <View style={{alignItems: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:60,
                    marginLeft: 5,
                    borderRadius:5,
                    marginTop:20
                   }}>
                    
                   <Text style={{fontSize:17,margin:10,marginLeft:15,marginRight:120 }}> filtrar recomendações</Text><Switch  thumbColor={'#ff5864'} value={true} />
                   </View>
                   <Text style={{marginLeft: 10, color:'grey'}}>Se você desativar esta opção, seu perfil será excluído</Text>
                   <Text style={{marginLeft: 10, color:'grey'}}>quando seus Matches fizerem uma busca nas</Text>
                   <Text style={{marginLeft: 10, color:'grey'}}>recomendações.</Text>
                   </View>


                   <View><Text style={{fontSize:17,fontWeight: "bold",margin:5 }}> Uso de dados</Text>
                   <View style={{alignItems: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:60,
                    marginLeft: 5,
                    borderRadius:5,
                    
                   }}>
                    
                   <Text style={{fontSize:17,margin:10}}> Reproduzir Vídeos automaticamente</Text>
                   </View>
                   
                   </View>


                   <View><View>
                   <View style={{alignItems: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    borderTopLeftRadius:5,
                    marginTop:20,
                    borderTopLeftRadius:5
                   }}>
                    
                    <Text style={{fontSize:17,fontWeight: "bold",margin:10,marginLeft:10,color:'#ff5864' }}> Perfil-Web</Text>
                   </View>
                   <View style={{justifyContent: 'center', flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:85,
                    marginLeft: 5,
                    borderBottomLeftRadius:5,
                    borderBottomRightRadius:5,
                    
                   }}>
                    
                   <Text style={{fontSize:17,margin:5 }}>Nome do Usuário</Text>
                   <Text style={{color:'grey',fontSize:17,margin:5,marginLeft:70}}>Garanta o seu  ></Text></View>
                   <Text style={{fontSize:10,marginLeft: 20, color:'grey', marginTop: -50}}>Crie um nome de usuário. Compartilhe seu nome de usuário. Deixe que</Text>
                   <Text style={{fontSize:10,marginLeft: 20, color:'grey', }}> pessoas do mundo todo deem Match com você diretamente no Tinder.</Text>
                   
                   </View>
                   </View>


                   <View><View><Text style={{fontSize:17,fontWeight: "bold",
                   margin:5,marginTop:30 }}> Top Picks</Text>
                   <View style={{alignItems: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    borderTopLeftRadius:5,
                    borderTopLeftRadius:5
                   }}>
                    
                    <Text style={{fontSize:17,fontWeight: "bold",margin:10,marginLeft:15,color:'#ff5864' }}>Gerenciar Top Picks</Text>
                   </View>
                   <View style={{justifyContent: 'center', flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    borderBottomLeftRadius:5,
                    borderBottomRightRadius:5,
                    
                   }}>
                    
                   <Text style={{fontSize:17,margin:5 }}>Configurações</Text>
                   <Text style={{color:'grey',fontSize:20,margin:5,marginLeft:200}}> ></Text></View>
                                      
                   </View>
                   </View>


                   <View><View><Text style={{fontSize:17,fontWeight: "bold",
                   margin:5,marginTop:30 }}>Confirmações de leitura</Text>
                   <View style={{alignItems: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    borderTopLeftRadius:5,
                    borderTopLeftRadius:5
                   }}>
                    
                    <Text style={{fontSize:17,fontWeight: "bold",margin:10,marginLeft:15,color:'#ff5864' }}>Gerenciar comfirmações de leitura</Text>
                   </View>
                   <View style={{justifyContent: 'center', flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    borderBottomLeftRadius:5,
                    borderBottomRightRadius:5,
                    
                   }}>
                    
                   <Text style={{fontSize:17,margin:5 }}>Settings</Text>
                   <Text style={{color:'grey',fontSize:20,margin:5,marginLeft:250}}> ></Text></View>
                                      
                   </View>
                   </View>


                   <View><View><Text style={{fontSize:17,fontWeight: "bold",margin:5,marginTop:30 }}> Match Time</Text>
                   <View style={{alignItems: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    borderTopLeftRadius:5,
                    borderTopLeftRadius:5
                   }}>
                    
                    <Text style={{fontSize:17,fontWeight: "bold",margin:10,marginLeft:15,color:'#ff5864' }}>Gerenciar Match Time</Text>
                   </View>
                   <View style={{justifyContent: 'center', flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    borderBottomLeftRadius:5,
                    borderBottomRightRadius:5,
                    
                   }}>
                    
                   <Text style={{fontSize:17,margin:5 }}>Configurações</Text>
                   <Text style={{color:'grey',fontSize:20,margin:5,marginLeft:200}}> ></Text></View>
                                      
                   </View>
                   </View>


                   <View><View><Text style={{fontSize:17,fontWeight: "bold",margin:5,marginTop:30 }}> Status de atividade</Text>
                   <View style={{alignItems: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    borderTopLeftRadius:5,
                    borderTopLeftRadius:5
                   }}>
                    
                    <Text style={{fontSize:17,fontWeight: "bold",margin:10,marginLeft:15,color:'#ff5864' }}>Status ativo recentemente</Text>
                   </View>
                   <View style={{justifyContent: 'center', flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    borderBottomLeftRadius:5,
                    borderBottomRightRadius:5,
                    
                   }}>
                    
                   <Text style={{fontSize:17,margin:5 }}>Configurações</Text>
                   <Text style={{color:'grey',fontSize:20,margin:5,marginLeft:200}}> ></Text></View>
                                      
                   </View>
                   </View>


                   <View><View><Text style={{fontSize:17,fontWeight: "bold",margin:5,marginTop:30 }}> Configurações</Text>
                   <View style={{alignItems: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    borderTopLeftRadius:5,
                    borderTopLeftRadius:5
                   }}>
                    
                    <Text style={{fontSize:17,fontWeight: "bold",margin:10,marginLeft:15,color:'#ff5864' }}>Notificações</Text>
                   </View>
                   <View style={{justifyContent: 'center', 
                    backgroundColor:'#fff',
                    width:360,
                    height:125,
                    marginLeft: 5,
                    borderBottomLeftRadius:5,
                    borderBottomRightRadius:5,
                    
                   }}>
                    
                   <Text style={{fontSize:17,margin:5,marginLeft:15 }}>E-mail</Text>
                   <Text style={{fontSize:17,margin:5,marginLeft:15 }}>Notificações por Push</Text>
                   <Text style={{fontSize:17,margin:5,marginLeft:15 }}>Equipe Tinder</Text>
                   </View>
                                      
                   </View>
                   </View>


                   <View><View>
                   <View style={{alignItems: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    borderTopLeftRadius:5,
                    marginTop:20,
                    borderTopLeftRadius:5
                   }}>
                    
                    <Text style={{fontSize:17,fontWeight: "bold",margin:10,marginLeft:10,color:'#ff5864' }}> Mostrar distâncias em</Text>
                    <Text style={{color:'black',fontSize:20,margin:5,marginLeft:105,fontWeight: "bold"}}>Km</Text>
                   </View>
                   <View style={{justifyContent: 'center', flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:60,
                    marginLeft: 5,
                    borderBottomLeftRadius:5,
                    borderBottomRightRadius:5,
                    
                   }}>
                    
                   <View style={{marginTop:5, width:150,height:40,backgroundColor:'#ff5864',borderRadius:5,justifyContent: 'center',alignItems:'center' }}>
                   <Text style={{color:'#fff',fontSize:20,margin:5,fontWeight: "bold"}}>Km</Text>
                   </View>
                   <View style={{marginTop:5, width:150,height:40,borderRadius:5 ,justifyContent: 'center',alignItems:'center' }}>
                   <Text style={{color:'grey',fontSize:20,margin:5,fontWeight: "bold"}}>Mi.</Text>
                     </View></View>
                   
                   </View>
                   </View>

                
                   
                   <View><Text style={{fontSize:17,fontWeight: "bold",margin:5,marginTop:30 }}> Conta de pagamento</Text>
                   <View style={{justifyContent: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:60,
                    marginLeft: 5,
                    borderTopLeftRadius:5,
                    borderTopLeftRadius:5
                   }}>
                    
                   <Text style={{fontSize:17,margin:15 }}> Gerencia conta de pagamento</Text>
                   <Text style={{color:'grey',fontSize:20,margin:15,marginLeft:70}}>></Text></View>
                   <View style={{justifyContent: 'center', flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:60,
                    marginLeft: 5,
                    marginTop:1,
                    borderBottomLeftRadius:5,
                    borderBottomRightRadius:5
                   }}>
                    
                   <Text style={{fontSize:17,margin:15 }}> Restaurar Compra</Text>
                   <Text style={{color:'grey',fontSize:20,margin:15,marginLeft:160}}>></Text></View></View>


                   <View><View><Text style={{fontSize:17,fontWeight: "bold",margin:5,marginTop:30 }}>Contato</Text>
                  
                   <View style={{justifyContent: 'center', alignItems:'center',
                    backgroundColor:'#fff',
                    width:360,
                    height:65,
                    marginLeft: 5,
                    borderBottomLeftRadius:5,
                    borderBottomRightRadius:5,
                    
                   }}>
                    
                   <Text style={{fontSize:17,margin:5}}>Ajuda e Suporte</Text>
                   
                   </View>
                                      
                   </View>
                   </View>


                   <View><View><Text style={{fontSize:17,fontWeight: "bold",margin:5,marginTop:30 }}> Comunidade</Text>
                   
                   <View style={{justifyContent: 'center', 
                    backgroundColor:'#fff',
                    width:360,
                    height:85,
                    marginLeft: 5,
                    borderBottomLeftRadius:5,
                    borderBottomRightRadius:5,
                    marginBottom:20
                   }}>
                    
                   <Text style={{fontSize:17,margin:5,marginLeft:10 }}>Regras da comunidade</Text>
                   <Text style={{fontSize:17,margin:5,marginLeft:10 }}>Dicas de segurança</Text>
                   </View>
                                      
                   </View>
                   </View>


                   <View style={{justifyContent: 'center',
                    backgroundColor:'#fff',
                    width:360,
                    height:65,
                    marginLeft: 5,
                    borderBottomLeftRadius:5,
                    borderBottomRightRadius:5,
                    marginBottom:20
                   }}>
                    
                   <Text style={{fontSize:17,margin:5,marginLeft:10}}>Compartilhar Tinder</Text>
                   
                   </View>


                   <View><View>
                   <View style={{alignItems: 'center',flexDirection:'row', 
                    backgroundColor:'#fff',
                    width:360,
                    height:45,
                    marginLeft: 5,
                    borderTopLeftRadius:5,
                    borderTopLeftRadius:5
                   }}>
                    
                    <Text style={{fontSize:17,fontWeight: "bold",margin:10,marginLeft:10,color:'#ff5864' }}>Legal</Text>
                   </View>
                   <View style={{justifyContent: 'center', 
                    backgroundColor:'#fff',
                    width:360,
                    height:125,
                    marginLeft: 5,
                    borderBottomLeftRadius:5,
                    borderBottomRightRadius:5,
                    marginBottom:20
                    
                   }}>
                    
                   <Text style={{fontSize:17,margin:5,marginLeft:10 }}>Lincenças</Text>
                   <Text style={{fontSize:17,margin:5,marginLeft:10 }}>Política de privacidade</Text>
                   <Text style={{fontSize:17,margin:5,marginLeft:10 }}>Termos de serviço</Text>
                   </View>
                                      
                   </View>
                   </View>

                   <View style={{justifyContent: 'center',alignItems:'center',
                    backgroundColor:'#fff',
                    width:360,
                    height:50,
                    marginLeft: 5,
                    borderBottomLeftRadius:5,
                    borderBottomRightRadius:5,
                    marginBottom:20
                   }}>
                    
                   <Text style={{fontSize:17,margin:5,marginLeft:10}}>Sair</Text>
                   
                   </View>

                   
                   
                   

              
              <Text></Text>
          
         
       
          <Text></Text>
          <Text></Text>
          


          </View>

            </View>


         
        </ScrollView>
      </View>
  );
}

function EditPerfil({ navigation }) {
  var radio_props = [
    {label: 'Homem', value: 0 },
    {label: 'Mulher', value: 1 }
  ];
  

    return (
        <View><View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor:"#fff",borderBottomWidth:1,borderColor:'#F6F7FB',
        width:450,
        height:49}}> 
        <View style={{justifyContent: 'center', alignItems: 'center',  width:60,   height:100,}}>
        <MaterialIcons name="arrow-back" size={25} color='tomato' onPress={() => navigation.goBack()}/>
        </View>
        
        <Text style={{fontSize:20,marginLeft:15,}}>Editar Perfil</Text>
                
            </View>
        
        <ScrollView style={{backgroundColor:'#f5f5f5'}}>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <View style={{
                    width:420,
                    height:600}}><View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
                    width:420,
                    height:190}}>
                        <View style={{backgroundColor:'#000', margin: 10, borderRadius:10,borderWidth: 3,borderStyle: 'dashed', borderColor:'#e1e4eb',
                    width:110,
                    height:170}}><View style={{
               
                      position: 'absolute',
                      top: 145,
                      left: 85,
                      width: 25,
                      height: 25,
                      borderRadius: 20,
                      backgroundColor: '#fff',
                      justifyContent: 'center',
                alignItems: 'center'
      
                    
                  }}><Text style={{color:'#FD5068',fontSize:15}}>X</Text></View> 
                        
                    </View><View style={{backgroundColor:'#eaedf2',margin: 10, borderRadius:10,borderWidth: 3,borderStyle: 'dashed', borderColor:'#e1e4eb',
                    width:110,
                    height:170}}><View style={{
               
                      position: 'absolute',
                      top: 145,
                      left: 85,
                      width: 25,
                      height: 25,
                      borderRadius: 20,
                      backgroundColor: '#FD5068',
                      justifyContent: 'center',
                alignItems: 'center'
      
                    
                  }}><Text style={{color:'#fff',fontSize:20}}>+</Text></View> 
                        
                    </View><View style={{backgroundColor:'#eaedf2',margin: 10, borderRadius:10,borderWidth: 3,borderStyle: 'dashed', borderColor:'#e1e4eb',
                    width:110,
                    height:170}}><View style={{
               
                      position: 'absolute',
                      top: 145,
                      left: 85,
                      width: 25,
                      height: 25,
                      borderRadius: 20,
                      backgroundColor: '#FD5068',
                      justifyContent: 'center',
                alignItems: 'center'
      
                    
                  }}><Text style={{color:'#fff',fontSize:20}}>+</Text></View> 
                        
                    </View>

                    </View><View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
                    width:420,
                    height:190}}>
                        <View style={{backgroundColor:'#eaedf2', margin: 10, borderRadius:10,borderWidth: 3,borderStyle: 'dashed', borderColor:'#e1e4eb',
                    width:110,
                    height:170}}><View style={{
               
                      position: 'absolute',
                      top: 145,
                      left: 85,
                      width: 25,
                      height: 25,
                      borderRadius: 20,
                      backgroundColor: '#FD5068',
                      justifyContent: 'center',
                alignItems: 'center'
      
                    
                  }}><Text style={{color:'#fff',fontSize:20}}>+</Text></View> 
                        
                    </View><View style={{backgroundColor:'#eaedf2',margin: 10, borderRadius:10,borderWidth: 3,borderStyle: 'dashed', borderColor:'#e1e4eb',
                    width:110,
                    height:170}}><View style={{
               
                      position: 'absolute',
                      top: 145,
                      left: 85,
                      width: 25,
                      height: 25,
                      borderRadius: 20,
                      backgroundColor: '#FD5068',
                      justifyContent: 'center',
                alignItems: 'center'
      
                    
                  }}><Text style={{color:'#fff',fontSize:20}}>+</Text></View> 
                        
                    </View><View style={{backgroundColor:'#eaedf2',margin: 10, borderRadius:10,borderWidth: 3,borderStyle: 'dashed', borderColor:'#e1e4eb',
                    width:110,
                    height:170}}><View style={{
               
                      position: 'absolute',
                      top: 145,
                      left: 85,
                      width: 25,
                      height: 25,
                      borderRadius: 20,
                      backgroundColor: '#FD5068',
                      justifyContent: 'center',
                alignItems: 'center'
      
                    
                  }}><Text style={{color:'#fff',fontSize:20}}>+</Text></View> 
                        
                    </View>

                    </View><View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
                    width:420,
                    height:190}}>
                        <View style={{backgroundColor:'#eaedf2', margin: 10, borderRadius:10,borderWidth: 3,borderStyle: 'dashed', borderColor:'#e1e4eb',
                    width:110,
                    height:170}}><View style={{
               
                      position: 'absolute',
                      top: 145,
                      left: 85,
                      width: 25,
                      height: 25,
                      borderRadius: 20,
                      backgroundColor: '#FD5068',
                      justifyContent: 'center',
                alignItems: 'center'
      
                    
                  }}><Text style={{color:'#fff',fontSize:20}}>+</Text></View> 
                        
                    </View><View style={{backgroundColor:'#eaedf2',margin: 10, borderRadius:10,borderWidth: 3,borderStyle: 'dashed', borderColor:'#e1e4eb',
                    width:110,
                    height:170}}><View style={{
               
                      position: 'absolute',
                      top: 145,
                      left: 85,
                      width: 25,
                      height: 25,
                      borderRadius: 20,
                      backgroundColor: '#FD5068',
                      justifyContent: 'center',
                alignItems: 'center'
      
                    
                  }}><Text style={{color:'#fff',fontSize:20}}>+</Text></View> 
                        
                    </View><View style={{backgroundColor:'#eaedf2',margin: 10, borderRadius:10,borderWidth: 3,borderStyle: 'dashed', borderColor:'#e1e4eb',
                    width:110,
                    height:170}}><View style={{
               
                      position: 'absolute',
                      top: 145,
                      left: 85,
                      width: 25,
                      height: 25,
                      borderRadius: 20,
                      backgroundColor: '#FD5068',
                      justifyContent: 'center',
                alignItems: 'center'
      
                    
                  }}><Text style={{color:'#fff',fontSize:20}}>+</Text></View> 
                        
                    </View>

                    </View>




                    </View>
                    <View style={{justifyContent: 'center', 
                    alignItems: 'center',
                    backgroundColor:'#FF5864',
                    width:350,
                    height:55,
                    borderRadius:15,
                    

                    }}><Text style={{fontSize:20, color:'#fff'}}>Adicionar mídia</Text></View>
                     <View style={{justifyContent: 'center', flexDirection: 'row',
                    alignItems: 'center',
                    backgroundColor:'#fff',
                    width:420,
                    height:55,
                    margin: 35
                    
                    

                    }}><Text style={{fontSize:20,
                    marginRight:200,color:'#FF5864'}}>Smart Photos</Text><Switch /></View>
                    <View>
                        
              
              
                   
                   <View style={{justifyContent: 'center', 
                    
                    width:400,
                    height:65,
                    marginLeft: 40
                   }}><Text style={{fontSize:17,
                    }}>O Smart Photos testa constantimente todas</Text>
                    <Text style={{fontSize:17,
                   }}> as fotos do seu perfil e escolhe a melhor.</Text>
                   
                   </View>
                   <View><Text style={{fontSize:17,fontWeight: "bold",margin:10,marginLeft:40 }}> Sobre Usuario</Text>
                   <View style={{justifyContent: 'center', 
                    backgroundColor:'#fff',
                    width:430,
                    height:60,
                    marginLeft: 30
                   }}>
                    
                   <Text style={{fontSize:17,margin:15 }}> Sobre Você</Text></View></View>
                   <View><Text style={{fontSize:17,fontWeight: "bold",marginTop:20,marginLeft:40,marginBottom:10 }}> Cargo</Text>
                   <View style={{justifyContent: 'center', 
                    backgroundColor:'#fff',
                    width:430,
                    height:60,
                    marginLeft: 30
                   }}>
                    
                   <Text style={{fontSize:17,margin:15 }}> Adicionar cago</Text></View></View>
                   <View><Text style={{fontSize:17,fontWeight: "bold",marginTop:20,marginLeft:40 ,marginBottom:10}}> Empresa</Text>
                   <View style={{justifyContent: 'center', 
                    backgroundColor:'#fff',
                    width:430,
                    height:60,
                    marginLeft: 30
                   }}>
                    
                   <Text style={{fontSize:17,margin:15 }}> Adicionar empresa</Text></View></View>
                   <View><Text style={{fontSize:17,fontWeight: "bold",marginTop:20,marginLeft:40 ,marginBottom:10}}> Escolaridade</Text>
                   <View style={{justifyContent: 'center', 
                    backgroundColor:'#fff',
                    width:430,
                    height:60,
                    marginLeft: 30
                   }}>
                    
                   <Text style={{fontSize:17,margin:15 }}> Adicionar escolaridade</Text></View></View>
                   <View><Text style={{fontSize:17,fontWeight: "bold",marginTop:20,marginLeft:40 ,marginBottom:10}}> Morando em</Text>
                   <View style={{justifyContent: 'center', 
                    backgroundColor:'#fff',
                    width:430,
                    height:60,
                    marginLeft: 30
                   }}>
                    
                   <Text style={{fontSize:17,margin:15 }}> Adicionar Cidade</Text></View></View>
                   <View><Text style={{fontSize:17,fontWeight: "bold",marginTop:20,marginLeft:40 ,marginBottom:10}}> Mostrar minhas fotos do Instagram</Text>
                   <View style={{justifyContent: 'center', 
                    backgroundColor:'#fff',
                    width:430,
                    height:60,
                    marginLeft: 30
                   }}>
                    
                   <Text style={{fontSize:17,margin:15, color:'#ff5864' }}> Conectar ao Instagram</Text></View></View>
                   <View><Text style={{fontSize:17,fontWeight: "bold",marginTop:20,marginLeft:40 ,marginBottom:10}}> Minhas Músicas</Text>
                   <View style={{justifyContent: 'center', 
                    backgroundColor:'#fff',
                    width:430,
                    height:60,
                    marginLeft: 30
                   }}>
                    
                   <Text style={{fontSize:17,margin:15 }}>Escolher sua Música </Text></View></View>
                   <View style={{justifyContent: 'center', 
                    
                    width:400,
                    height:65,
                    marginLeft: 40
                   }}><Text style={{fontSize:17,
                    }}>Controle como você compartilha sua música</Text>
                    <Text style={{fontSize:17,
                   }}> do Spotify no Feed em Configurações.</Text>
                   
                   </View>

                   <View><Text style={{fontSize:17,fontWeight: "bold",marginTop:20,marginLeft:40 ,marginBottom:10}}> Meus artistas preferidos no Spotify</Text>
                   <View style={{justifyContent: 'center', 
                   alignItems:'center',
                    backgroundColor:'#fff',
                    width:430,
                    height:60,
                    
                   }}>
                    
                   <Text style={{fontSize:17,margin:15,color:'#ff5864' }}> Adicionar Spotify ao seu perfil</Text></View></View>
                   <View style={{justifyContent: 'center', 
                    
                    width:400,
                    height:65,
                    marginLeft: 40
                   }}><Text style={{fontSize:17,
                    }}>Controle como você compartilha sua música</Text>
                    <Text style={{fontSize:17,
                   }}> do Spotify no Feed em Configurações.</Text>
                   
                   </View>

                   <View><Text style={{fontSize:17,fontWeight: "bold",marginTop:20,marginLeft:40,marginBottom:10 }}> Sexo</Text>
                   <View style={{justifyContent: 'center', 
                   
                    backgroundColor:'#fff',
                    width:430,
                    height:65,
                    
                    
                   }}><View style={{marginLeft:40}}>
                    <RadioForm buttonColor={'grey'}
                    
                    buttonOuterColor={'yellow'}
                    buttonSize={7}
                    labelStyle={{ fontSize: 17}}
                    radio_props={radio_props}
                    initial={0}
                    
                    /></View>
                   </View></View>
                   <View><Text style={{fontSize:17,
                    fontWeight: "bold",marginTop:20,marginLeft:40,marginBottom:10 }}> Controle seu perfil</Text>
                   <View style={{justifyContent: 'center', flexDirection: 'row',
                    backgroundColor:'#fff',
                    width:430,
                    height:35,
                    marginLeft: 20
                   }}>
                    
                   <Text style={{fontSize:17,marginRight:150 }}>Não mostre minha idade</Text><Switch/></View>
                   <View style={{justifyContent: 'center', flexDirection: 'row',
                    backgroundColor:'#fff',
                    width:430,
                    height:35,
                    marginLeft: 20
                   }}>
                    
                   <Text style={{fontSize:17, marginRight:99  }}>Tornar minha distância invisivel</Text><Switch/></View></View>
                   
                   
                   

              
              <Text></Text>
          
         
       
          <Text></Text>
          <Text></Text>
          


          </View>

            </View>


         
        </ScrollView>
      </View>
    );
  }

function AddMidia({ navigation }) {
    return (
      <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'flex-start', backgroundColor:'#fff'}}>
             <View style={{
        width:500,
        height:600}}>

            <View style={{flexDirection: 'row',
        width:500,
        height:50}}><View style={{ flexDirection: 'row', alignItems: 'center' ,borderBottomWidth:1,borderColor:'#F6F7FB',
        width:450,
        height:49}}> 
        <View style={{justifyContent: 'center', alignItems: 'center',  width:60,   height:100,}}>
        <MaterialIcons name="arrow-back" size={25} color='tomato' onPress={() => navigation.goBack()}/>
        </View>
        
        <Text style={{fontSize:20,marginLeft:15,}}>Selecionar Fonte</Text>
                
            </View>
                
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center',
        width:450,
        height:100}}> 
        <View style={{marginLeft:15, backgroundColor:'#F6F7FB',justifyContent: 'center', alignItems: 'center',  width:70,   height:70}}>
        <Icon name="camera" size={30} color='grey'/>
        </View>
        
        <Text style={{fontSize:20,marginLeft:25}}>Câmera</Text><View style={{
               
               position: 'absolute',
               top: 100,
               left: 110,
               width: 350,
               height: 2,
               
               backgroundColor: '#F6F7FB',
               justifyContent: 'center',
         alignItems: 'center'

             
           }}></View> 
                
            </View>
            <View style={{flexDirection: 'row',alignItems: 'center',
        width:500,
        height:100}}>
           
            <View  style={{marginLeft:15, backgroundColor:'#F6F7FB',justifyContent: 'center', alignItems: 'center',  width:70,   height:70}}><Icon name="image" size={40} color='grey'/></View>
            
            <Text style={{fontSize:20,marginLeft:25}}>Galeria</Text><View style={{
               
               position: 'absolute',
               top: 100,
               left: 110,
               width: 350,
               height: 2,
               
               backgroundColor: '#F6F7FB',
               justifyContent: 'center',
         alignItems: 'center'

             
           }}></View> 
                
            </View>
            </View>
        
      </View>
    );
  }



const Stack = createStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator screenOptions={{
        header: () => null
      }}>
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="Forget" component={Forget}/>
      <Stack.Screen name="Profile" component={ProfileScreen} />
      <Stack.Screen name="SettingsScreen" component={SettingsScreen} />
      <Stack.Screen name="EditPerfil" component={EditPerfil} />
      <Stack.Screen name="AddMidia" component={AddMidia} />

    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <MyStack />
    </NavigationContainer>
  );
}