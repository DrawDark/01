import React, { useContext } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { Text, TouchableOpacity, View,ImageBackground,StyleSheet,StatusBar } from "react-native";
import { AuthContext } from "./AuthProvider";
import { HomeParamList, HomeStackNavProps } from "./HomeParamList";
import { addProductRoutes } from "./addProductRoutes";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icons from 'react-native-vector-icons/Feather';
import EntypoIcons from 'react-native-vector-icons/Entypo';

interface HomeStackProps {}
const image = { uri: "https://img.r7.com/images/2017/02/13/2hzeldwdrl_yh9jubknu_file" };
const Stack = createStackNavigator<HomeParamList>();
 
function Feed({ navigation }: HomeStackNavProps<"Feed">) {
  
  return (
        
    <View style={{
      alignItems: 'center', 
      justifyContent: 'flex-start',
      backgroundColor:'#F6F7FB' }}>
        <View style={{
      width: 500,
      height: 500,
      borderWidth: 1,
      borderColor:'#000',
    }}>
      <ImageBackground 
      source={image} 
      style={{
        
        height:498
      }}/>
  
    <View
     style={{position: 'absolute',
     top: 385,
     left: 60,
     width: 400,
     height: 80,
     borderRadius: 20,
     justifyContent: 'center'
     }}>
       <View style={{flexDirection:'row',
       alignItems: 'baseline',
       width: 300
       }}>
         
         <Text  style={{  fontSize: 40,color:'#fff'  }}>Han</Text>

          <Text style={{  fontSize: 30,color:'#fff',marginLeft:10}}>26</Text>
          
          </View>
          <View style={{
            width:300,
            alignItems: 'center',
            flexDirection: 'row'
            
          }}>
            <Icon name="briefcase" size={12} color='#fff'/>
            <Text style={{  fontSize: 15,color:'#fff',marginLeft:10}}>Photographer </Text></View>
            <View style={{
            width:300,
            alignItems: 'center',
            flexDirection: 'row'
            
          }}>
            <Icon name="university" size={12} color='#fff'/>
            <Text style={{  fontSize: 15,color:'#fff' ,marginLeft:10}}>Hanci University </Text></View>
            <View style={{
            width:300,
            alignItems: 'center',
            flexDirection: 'row'
            
          }}>
            <EntypoIcons name="location-pin" size={12} color='#fff'/>
            <Text style={{  fontSize: 15,color:'#fff',marginLeft:10}}>2 miles away </Text></View>
            

    </View>

  </View>


 <View style={{height: 20}}></View>

<View style={{flexDirection:'row'}}>
  
  <View style={{width: 40}}></View>
  <View style={{ justifyContent: 'center',
          alignItems: 'center',
          borderRadius:50,
          height:80,
          width:80,
  backgroundColor: '#ffff'}}>
  <Icons name="x" size={50} color='#FD4A6C'/></View>

  <View style={{width: 20}}></View>

  <View style={{ justifyContent: 'center',
          alignItems: 'center',
          borderRadius:50,
          height:70,
          width:70,
  backgroundColor: '#ffff'}}>
  <Icon name="star" size={40} color='#01BAFF'/></View>

  <View style={{width: 20}}></View>

  <View style={{justifyContent: 'center',
          alignItems: 'center',
          borderRadius:50,
          height:80,
          width:80,
  backgroundColor: '#ffff'}}>
  <MaterialCommunityIcons name="heart" size={50} color='#00E9BA'/></View>
  
  <View style={{width: 40}}></View>

  </View>
  <StatusBar  
        backgroundColor='tomato'  
        barStyle='light-content'  
    />  
    <View style={{flex:1}}>  
         
    </View>  
    
  </View>

  );
}




export const HomeStack: React.FC<HomeStackProps> = ({}) => {
  const { logout } = useContext(AuthContext);
  return (
    <Stack.Navigator 
    screenOptions={{
      header: () => null
    }}
    initialRouteName="Feed">
      {addProductRoutes(Stack)}
      <Stack.Screen
        name="Feed"
        options={{
          headerRight: () => {
            return (
              <TouchableOpacity>
                
              </TouchableOpacity>
            );
          }
        }}
        component={Feed}
      />
    </Stack.Navigator>
  );
};

HomeStack.navigationOptions={  
  tabBarIcon:({tintColor, focused})=>(  
      <Icon  
          name={focused ? 'ios-person' : 'md-person'}  
          color={tintColor}  
          size={25}  
      />  
  )  
}  
