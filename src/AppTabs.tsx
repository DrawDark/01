import { HomeStack } from "./HomeStack";
import { SearchStack } from "./SearchStack";
import { PerfilStack } from "./PerfilStack";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import * as React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { Settings } from "react-native";
import {SettingsStack} from './SettingsStack'



interface AppTabsProps {}

const Tab = createMaterialTopTabNavigator();





export const AppTabs: React.FC<AppTabsProps> = ({}) => {
  return (
    
      <Tab.Navigator 
      
        tabBarOptions={{
        fontSize: 20 ,
        showIcon: true, 
        showLabel:false,
        activeTintColor: "tomato",
        inactiveTintColor: "gray",
        
        
        
      }} initialRouteName='HomeStack'>

        <Tab.Screen name="Perfil" component={PerfilStack} 
        options={{
          
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="account" color={color} size={30} />
          ),
        }}/>
        <Tab.Screen name="Main" component={HomeStack}
         options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="heart" color={color} size={26} />
          ),
        }}/>
        <Tab.Screen name="Chat" component={SearchStack}  options={{
          tabBarLabel: 'Chat',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="chat" color={color} size={26} />
          ),
        }}/>
      </Tab.Navigator>
    
  );
}
