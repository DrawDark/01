import React, { useContext } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { Text, TouchableOpacity, View, Button } from "react-native";
import { AuthContext } from "./AuthProvider";
import { PerfilParamList, PerfilStackNavProps } from "./PerfilParamList";
import { addProductRoutes } from "./addProductRoutes";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/FontAwesome';



interface PerfilStackProps {}

const Stack = createStackNavigator<PerfilParamList>();

function Perfil({ navigation }) {
  
  return (
    <View style={{ flex: 1, 
      alignItems: 'center', 
      justifyContent: 'flex-start',
      backgroundColor:'#fff' }}>

    
    <View style={{
          
          
          backgroundColor:'#fff',
          alignItems: 'center', 
      }}>
              <View style={{
          width: 550,
          height: 380,
          borderBottomLeftRadius:250,
          borderBottomRightRadius:250,
          backgroundColor:'#ffff',
          alignItems: 'center', 
          justifyContent: 'flex-start',
          marginTop:20}}>

            <MaterialCommunityIcons name="account-circle" size={150} color='#3d3d3d'/>
            <Text style={{  fontSize: 25,}}>Nome do Usuario </Text>
            <Text style={{  fontSize: 20,}}>Informações Adicionais </Text>
            <Text style={{  fontSize: 20,}}>Informações Adicionais </Text>
            
            <View style={{height: 30}}></View>
            
            <View style={{flexDirection:'row',
          alignItems: 'center'}}>
              
              <View style={{flexDirection:'column',
          alignItems: 'center'}} >
          <View style={{backgroundColor: '#F6F7FB',
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius:50,
          height:60,
          width:60
          }}>
            <MaterialCommunityIcons name="settings" size={30} color='#C4C7D0' onPress={() => navigation.navigate('SettingsScreen')}/></View>
               
              <Text style={{color:'#616161'}}  >Configurações</Text>
              <Text> </Text>
             </View>

             <View style={{width: 120}}></View>

          

          

          <View style={{flexDirection:'column',
          alignItems: 'center'}}>
          <View style={{backgroundColor: '#F6F7FB',
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius:50,
          height:60,
          width:60
           }}>
            <Icon name="edit" size={30} color='#C4C7D0' onPress={() => navigation.navigate('EditPerfil')}/></View>
      <Text style={{color:'#616161'}}>   Editar     </Text>
      <Text style={{color:'#616161'}}>Informações</Text>
          </View>
      
      
      
      </View>
     
      <View style={{
         position: 'absolute',
         top: 320,
         left: 200,
         width: 150,
         height: 100,
         borderRadius: 20,
         justifyContent: 'center',
         flexDirection:'column',
         alignItems: 'center'}}>
            
             <View style={{flexDirection:'column',
          alignItems: 'center'}}></View>

          <View style={{backgroundColor: '#FD5068',
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius:50,
          height:60,
          width:60
          }}>
             <Icon name="camera" size={30} color='#fff' onPress={() => navigation.navigate('AddMidia')}/>
            <View style={{
               
                position: 'absolute',
                top: 42,
                left: 42,
                width: 18,
                height: 18,
                borderRadius: 20,
                backgroundColor: '#fff',
                justifyContent: 'center',
          alignItems: 'center'

              
            }}><Text style={{color:'#FD5068',fontWeight: "bold"}}>+</Text></View> 
            
            </View>
            
            
          

      <Text style={{color:'#616161'}} > Adicionar Midia </Text>
      <Text></Text>
          </View>

          </View>
         
          </View>
    
          
  </View>
  );
}
//<Icon name="power-off" size={40} color='#3d3d3d' onPress={() => { logout(); }}/>

export const PerfilStack: React.FC<PerfilStackProps> = ({}) => {
  const { logout } = useContext(AuthContext);
  return (
    <Stack.Navigator 
    screenOptions={{
      header: () => null
    }}
    initialRouteName="Perfil">
      {addProductRoutes(Stack)}
      <Stack.Screen
        name="Perfil"
        options={{
          headerRight: () => {
            return (
              <TouchableOpacity
                onPress={() => {
                  logout();
                }}
              >
                <Text>LOGOUT </Text>
              </TouchableOpacity>
            );
          }
        }}
        component={Perfil}
      />
    </Stack.Navigator>
  );
};

PerfilStack.navigationOptions={  
  tabBarIcon:({tintColor, focused})=>(  
      <Icon  
          name={focused ? 'ios-person' : 'md-person'}  
          color={tintColor}  
          size={25}  
      />  
  )  
}  
