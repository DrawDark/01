import React, { useContext } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { Text, TouchableOpacity, View, Button } from "react-native";
import { AuthContext } from "./AuthProvider";
import { PerfilParamList, PerfilStackNavProps } from "./PerfilParamList";
import { addProductRoutes } from "./addProductRoutes";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/FontAwesome';



interface SettingsStackProps {}

const Stack = createStackNavigator<SettingsParamList>();

function Settings({ navigation }: SettingsStackNavProps<"Settings">) {
  
  return (
    <View style={{ flex: 1, 
      alignItems: 'center', 
      justifyContent: 'flex-start',
      backgroundColor:'#fff' }}>

       
          
  </View>
  );
}
export const SettingsStack: React.FC<SettingsStackProps> = ({}) => {
  const { logout } = useContext(AuthContext);
  return (
    <Stack.Navigator 
    screenOptions={{
      header: () => null
    }}
    initialRouteName="Settings">
      {addProductRoutes(Stack)}
      <Stack.Screen
        name="Settings"
        options={{
          headerRight: () => {
            return (
              <TouchableOpacity
                onPress={() => {
                  logout();
                }}
              >
                <Text>LOGOUT </Text>
              </TouchableOpacity>
            );
          }
        }}
        component={Settings}
      />
    </Stack.Navigator>
  );
};

SettingsStack.navigationOptions={  
  tabBarIcon:({tintColor, focused})=>(  
      <Icon  
          name={focused ? 'ios-person' : 'md-person'}  
          color={tintColor}  
          size={25}  
      />  
  )  
}  