import React, { useContext } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { AuthParamList, AuthNavProps } from "./AuthParamList";
import { AuthContext } from "./AuthProvider";
import { Button, Text ,View} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Icon, Fontisto} from 'react-native-vector-icons/FontAwesome';
import { AppTabs } from "./AppTabs";

interface AuthStackProps {}

const Stack = createStackNavigator<AuthParamList>();

function Login({ navigation }: AuthNavProps<"Login">) {
  const { login } = useContext(AuthContext);
  return (
    <View style={{ flex: 1, 
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor:'#FD5068' }}>
        <View style={{flexDirection:'row',
      alignItems: 'center',
      justifyContent: 'center'}}>
        <MaterialCommunityIcons name="fire" size={80} color='#fff'/> 
        <Text style={{  fontSize: 50,    color:'#ffff' }}>tinder</Text>
        </View>
        <View style={{
          height:50
        }}></View>
        <Text style={{  fontSize: 15,    color:'#ffff' }}>By tapping Log In, you agree with</Text>
        <Text style={{  fontSize: 15,    color:'#ffff' }}>our Terms of Service and Privacy</Text>
        <Text style={{  fontSize: 15,    color:'#ffff' }}>Policy</Text>
        <View style={{
          height:30
        }}></View>

        <View style={{
          alignItems: 'center',
          justifyContent: 'center',
          width:350,
          height:65,
          backgroundColor:'#3b5998',
          borderRadius:30
        }}
        >
<Text style={{  fontSize: 20,    color:'#ffff' }}    onPress={() => {     login();     }}  >LOG IN WITH FACEBOOK</Text>

        </View>
        <View style={{
          height:10
        }}></View>

        <View style={{
          alignItems: 'center',
          justifyContent: 'center',
          width:350,
          height:65,
          borderWidth: 3,
          borderColor:'#FFFF',
          borderRadius:30
        }}
        >
<Text style={{  fontSize: 20,    color:'#ffff' }}    onPress={() => {    navigation.navigate("Forget")    }}  >LOG IN WITH PHONE NUMBER</Text>

        </View>
        <View style={{
          height:50
        }}></View>
        <Text style={{  fontSize: 15,    color:'#ffff' }}>Trouble Logging in?</Text>
        <View style={{
          height:30
        }}></View>

        <Text style={{  fontSize: 15,    color:'#ffff' }}>We don't post anything to</Text>
        <Text style={{  fontSize: 15,    color:'#ffff' }}>Facebook</Text>

        
      
     
  </View>
    
      
      //<Button        title="log me in"        onPress={() => {          login();        }}      />
      //<Button title="go to register"   onPress={() => {   navigation.navigate("Register");        }}  />
    
  );
}



function Forget({ navigation, route }: AuthNavProps<'Forget'>) {
  return (
    <View style={{ flex: 1, alignItems: 'center', backgroundColor:'#ffff' }}>
      <View style={{
        height:50
      }}>

      </View>
      <View style={{
        width:350,
        height:100
        
      }}>
<Text style={{ fontSize: 20, color:'#000' }}>Meu número é</Text>
      </View>

      
        
       <View style={{flexDirection:'row',
          alignItems: 'center',
          justifyContent: 'center',}}>
      <Text title="EmailRecuperar" 
                   style={{
                   width: 60,
                   height: 40,                        
                   backgroundColor:'#ffff',
                   fontSize:20
                  }}>
  
                  +55 <MaterialCommunityIcons name="chevron-down" size={20} color='#000'/>
                  </Text>
                  <View style={{
                    width:30,
                    height:3,
                    
                  }}></View>

        <TextInput title="EmailRecuperar" 
                   style={{ width: 250,
                   height: 40,                        
                   backgroundColor:'#ffff',
                   fontSize:20 
                  }}>
  
                  Número de telefone
                  </TextInput></View>
                  <View style={{flexDirection:'row',
          alignItems: 'center',
          justifyContent: 'center',}}>
            
            <View style={{
                    width:60,
                    height:3,
                    backgroundColor:'#000'
                  }}></View>
                  <View style={{
                    width:30,
                    height:3,
                    
                  }}></View>
                  <View style={{
                    width:250,
                    height:3,
                    backgroundColor:'#000'
                  }}></View>
                  </View>
                  <View style={{
                    width:380,
                    height:30}}></View>
                  <View style={{
                    width:350,
                    height:150,
                    
                  }}><Text>Quando você tocar em Continuar, o Tinder lhe enviará </Text>
                  <Text>uma mensagem de texto com o código de verificação. </Text>
                  <Text>Tarifas de mensagens e dados podem ser aplicáveis. O </Text>
                  <Text>número de telefone confirmado pode ser utilizado para </Text>
                  <Text>entrar no Tinder. Saiba o que acontece se seu número mudar.</Text>
                  </View>
  
                  <View style={{
          alignItems: 'center',
          justifyContent: 'center',
          width:350,
          height:65,
          borderWidth: 3,
          borderColor:'#000',
          borderRadius:30
        }}
        >
<Text style={{  fontSize: 20,    color:'#000' }}    onPress={() => {    navigation.navigate("Login")    }}  >CONTINUAR</Text>

        </View>
        </View>
  );
}

export const AuthStack: React.FC<AuthStackProps> = ({}) => {
  return (
    <Stack.Navigator
      screenOptions={{
        header: () => null
      }}
      initialRouteName="Login"
    >
      <Stack.Screen
        options={{
          headerTitle: "Sign In"
        }}
        name="Login"
        component={Login}
      />
     <Stack.Screen  options={{
        headerTitle: "Sign Up"
      }}
      name="AppTabs"
      component={AppTabs}> 
      <AppTabs/>

      </Stack.Screen>

       <Stack.Screen
        options={{
          headerTitle: "Recuperar Senha"
        }}
        name="Forget"
        component={Forget}
      />

    </Stack.Navigator>
  );
};
