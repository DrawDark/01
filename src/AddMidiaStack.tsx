import React, { useContext } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { Text, TouchableOpacity, View } from "react-native";
import { AuthContext } from "./AuthProvider";
import { PerfilParamList, PerfilStackNavProps } from "./PerfilParamList";
import { addProductRoutes } from "./addProductRoutes";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/FontAwesome';


interface AddMidiaStackProps {}

const Stack = createStackNavigator<AddMidiaParamList>();

function AddMidia({ navigation }: AddMidiaStackNavProps<"AddMidia">) {
  
  return (
    <View style={{ flex: 1, 
      alignItems: 'center', 
      justifyContent: 'flex-start',
      backgroundColor:'#F6F7FB' }}>

    
    
          
  </View>
  );
}
//<Icon name="power-off" size={40} color='#3d3d3d' onPress={() => { logout(); }}/>

export const AddMidialStack: React.FC<AddMidiaStackProps> = ({}) => {
  const { logout } = useContext(AuthContext);
  return (
    <Stack.Navigator 
    screenOptions={{
      //header: () => null
    }}
    initialRouteName="AddMidia">
      {addProductRoutes(Stack)}
      <Stack.Screen
        name="Add Midia"
        options={{
          headerRight: () => {
            return (
              <TouchableOpacity
                onPress={() => {
                  logout();
                }}
              >
                <Text>LOGOUT </Text>
              </TouchableOpacity>
            );
          }
        }}
        component={AddMidia}
      />
    </Stack.Navigator>
  );
};

AddMidiaStack.navigationOptions={  
  tabBarIcon:({tintColor, focused})=>(  
      <Icon  
          name={focused ? 'ios-person' : 'md-person'}  
          color={tintColor}  
          size={25}  
      />  
  )  
}  