import { StackNavigationProp } from "@react-navigation/stack";
import { RouteProp } from "@react-navigation/native";
import { ProductParamList } from "./ProductParamList";

export type PerfilParamList = {
  Perfil: undefined;
} & ProductParamList;

export type PerfilStackNavProps<T extends keyof PerfilParamList> = {
  navigation: StackNavigationProp<PerfilParamList, T>;
  route: RouteProp<PerfilParamList, T>;
};
